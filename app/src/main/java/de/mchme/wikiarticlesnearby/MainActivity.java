package de.mchme.wikiarticlesnearby;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import de.mchme.wikiarticlesnearby.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }
}
