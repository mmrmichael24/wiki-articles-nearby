package de.mchme.wikiarticlesnearby.service.articlepersistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface IWikipediaArticleDao {

    @Query("SELECT * FROM articles")
    List<WikipediaArticleEntity> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<WikipediaArticleEntity> wikipediaArticleEntities);

    // https://stackoverflow.com/questions/47538857/android-room-delete-with-parameters
    @Query("DELETE FROM articles WHERE Insertvalue < :ts")
    void deleteByTimestamp(long ts);
}
