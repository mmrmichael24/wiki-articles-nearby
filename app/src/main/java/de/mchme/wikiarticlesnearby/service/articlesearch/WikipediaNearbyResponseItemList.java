package de.mchme.wikiarticlesnearby.service.articlesearch;

import java.util.List;

public class WikipediaNearbyResponseItemList {

    private List<WikipediaNearbyResponseItem> geosearch ;

    public List<WikipediaNearbyResponseItem> getGeosearch() {
        return geosearch;
    }

    public void setGeosearch(List<WikipediaNearbyResponseItem> geosearch) {
        this.geosearch = geosearch;
    }
}
