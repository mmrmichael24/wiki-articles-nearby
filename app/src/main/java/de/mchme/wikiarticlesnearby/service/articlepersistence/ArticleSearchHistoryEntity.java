package de.mchme.wikiarticlesnearby.service.articlepersistence;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(tableName = "history" , primaryKeys = { "Geohash" })
public class ArticleSearchHistoryEntity {

    @NonNull
    public String Geohash ;

    @NonNull
    public int Resultcount ;

    @NonNull
    public long Insertvalue ;

}
