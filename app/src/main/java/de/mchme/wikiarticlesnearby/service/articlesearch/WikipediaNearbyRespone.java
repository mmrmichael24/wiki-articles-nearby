package de.mchme.wikiarticlesnearby.service.articlesearch;

public class WikipediaNearbyRespone {

    private String batchcomplete ;
    private WikipediaNearbyResponseItemList query ;

    public String getBatchcomplete() {
        return batchcomplete;
    }

    public void setBatchcomplete(String batchcomplete) {
        this.batchcomplete = batchcomplete;
    }

    public WikipediaNearbyResponseItemList getQuery() {
        return query;
    }

    public void setQuery(WikipediaNearbyResponseItemList query) {
        this.query = query;
    }
}
