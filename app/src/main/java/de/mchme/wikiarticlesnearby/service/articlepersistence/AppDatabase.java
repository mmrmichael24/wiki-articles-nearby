package de.mchme.wikiarticlesnearby.service.articlepersistence;

import androidx.room.Database;
import androidx.room.RoomDatabase;

// https://developer.android.com/training/data-storage/room

@Database(entities = {WikipediaArticleEntity.class , ArticleSearchHistoryEntity.class }, version = 1,  exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract IWikipediaArticleDao getWikipediaArticleDao();
    public abstract IArticleSearchHistoryDao getArticleSearchHistoryDao();

}
