package de.mchme.wikiarticlesnearby.service.articlepersistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface IArticleSearchHistoryDao {

    @Query("SELECT * from history where Geohash = :geohash and Insertvalue > :ts")
    List<ArticleSearchHistoryEntity> findByGeohash(String geohash, long ts) ;

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ArticleSearchHistoryEntity> historyEntities);

    // https://stackoverflow.com/questions/47538857/android-room-delete-with-parameters
    @Query("DELETE FROM history WHERE Insertvalue < :ts")
    void deleteByTimestamp(long ts);

}
