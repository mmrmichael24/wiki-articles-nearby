package de.mchme.wikiarticlesnearby.service.repo;

import android.content.Context;

import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;

import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;
import de.mchme.wikiarticlesnearby.service.articlepersistence.AppDatabase;
import de.mchme.wikiarticlesnearby.service.articlepersistence.ArticleSearchHistoryEntity;
import de.mchme.wikiarticlesnearby.service.articlepersistence.IArticleSearchHistoryDao;
import de.mchme.wikiarticlesnearby.service.articlepersistence.IWikipediaArticleDao;
import de.mchme.wikiarticlesnearby.service.articlepersistence.WikipediaArticleEntity;

public class ArticleDatabaseRepo {

    // Articles

    public static void insertNewWikiArticles( List<WikipediaArticle> wikipediaArticleList , Context ctx ) {
        List<WikipediaArticleEntity> wikipediaArticleEntityList = new ArrayList<WikipediaArticleEntity>() ;

        final long ts = getTimeStamp();

        for( WikipediaArticle wikipediaArticle : wikipediaArticleList) {
            WikipediaArticleEntity wikipediaArticleEntity = new WikipediaArticleEntity();
            wikipediaArticleEntity.pageid = wikipediaArticle.getPageid();
            wikipediaArticleEntity.title = wikipediaArticle.getTitle();
            wikipediaArticleEntity.lat = wikipediaArticle.getLat();
            wikipediaArticleEntity.lon = wikipediaArticle.getLon();
            wikipediaArticleEntity.link = wikipediaArticle.getLink();
            wikipediaArticleEntity.Insertvalue = ts ;

            wikipediaArticleEntityList.add(wikipediaArticleEntity) ;
        }

        AppDatabase db = getAppDatabase(ctx) ;
        IWikipediaArticleDao wikipediaArticleDao = db.getWikipediaArticleDao();
        wikipediaArticleDao.insertAll(wikipediaArticleEntityList);
        db.close();
    }

    public static List<WikipediaArticle> getWikipediaArticles(Context ctx) {
        List<WikipediaArticle> wikipediaArticleList = new ArrayList<WikipediaArticle>() ;

        AppDatabase db = getAppDatabase(ctx) ;
        IWikipediaArticleDao wikipediaArticleDao = db.getWikipediaArticleDao();

        List<WikipediaArticleEntity> wikipediaArticleEntityList = wikipediaArticleDao.getAll() ;

        for( WikipediaArticleEntity wikipediaArticleEntity : wikipediaArticleEntityList) {
            WikipediaArticle wikipediaArticle = new WikipediaArticle();
            wikipediaArticle.setLink(wikipediaArticleEntity.link);
            wikipediaArticle.setTitle(wikipediaArticleEntity.title);
            wikipediaArticle.setPageid(wikipediaArticleEntity.pageid);
            wikipediaArticle.setLon(wikipediaArticleEntity.lon);
            wikipediaArticle.setLat(wikipediaArticleEntity.lat);

            wikipediaArticleList.add(wikipediaArticle) ;
        }

        db.close();

        return wikipediaArticleList ;
    }

    public static void deleteArticlesByTime( int hours, Context ctx ) {

        final long ts = getTimeStamp() - hours * 60 * 60 ;

        AppDatabase db = getAppDatabase(ctx) ;
        IWikipediaArticleDao wikipediaArticleDao = db.getWikipediaArticleDao();

        wikipediaArticleDao.deleteByTimestamp(ts);

        db.close();

    }

    // History
    public static void insertNewHistoryEntry(String geohash, int articlesfound, Context ctx) {

        AppDatabase db = getAppDatabase(ctx) ;
        IArticleSearchHistoryDao articleSearchHistoryDao = db.getArticleSearchHistoryDao();

        List<ArticleSearchHistoryEntity> articleSearchHistoryEntities = new ArrayList<ArticleSearchHistoryEntity>() ;

        ArticleSearchHistoryEntity historyEntity = new ArticleSearchHistoryEntity();

        historyEntity.Geohash = geohash;
        historyEntity.Insertvalue = getTimeStamp() ;
        historyEntity.Resultcount = articlesfound ;

        articleSearchHistoryEntities.add(historyEntity) ;

        articleSearchHistoryDao.insertAll(articleSearchHistoryEntities);

        db.close();
    }

    public static Boolean isCacheExpired( String geohash, int cacheInHours , Context ctx  ) {
        Boolean expired = true ;

        final long ts = getTimeStamp() - cacheInHours * 60 * 60 ;

        AppDatabase db = getAppDatabase(ctx) ;
        IArticleSearchHistoryDao articleSearchHistoryDao = db.getArticleSearchHistoryDao();

        List<ArticleSearchHistoryEntity> historyEntities = articleSearchHistoryDao.findByGeohash(geohash, ts) ;

        if ( historyEntities != null && historyEntities.size() > 0) {
            expired = false;
        }

        db.close();

        return  expired ;
    }

    // some utils

    private static AppDatabase getAppDatabase( Context ctx ) {
        return Room.databaseBuilder(ctx, AppDatabase.class, "wikiarticles").build();
    }

    private static long getTimeStamp() {
        // seconds
        return  System.currentTimeMillis()/1000;
    }

}
