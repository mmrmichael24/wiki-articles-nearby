package de.mchme.wikiarticlesnearby.service.articlesearch;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IWikipediaNearbySearch {

    // https://de.wikipedia.org/w/api.php?action=query&list=geosearch&gscoord=37.7891838|-122.4033522&format=json&gsradius=760

    @GET("w/api.php")
    Call<WikipediaNearbyRespone> searchWikipediaNearby(@Query("action") String action ,
                                                       @Query("list") String list ,
                                                       @Query("gscoord") String gscord ,
                                                       @Query("format") String format ,
                                                       @Query("gslimit") int limit ,
                                                       @Query("gsradius") int radius );
}
