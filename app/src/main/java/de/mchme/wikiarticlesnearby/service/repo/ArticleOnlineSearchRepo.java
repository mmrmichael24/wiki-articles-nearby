package de.mchme.wikiarticlesnearby.service.repo;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;
import de.mchme.wikiarticlesnearby.service.articlesearch.IWikipediaNearbySearch;
import de.mchme.wikiarticlesnearby.service.articlesearch.WikipediaNearbyRespone;
import de.mchme.wikiarticlesnearby.service.articlesearch.WikipediaNearbyResponseItem;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ArticleOnlineSearchRepo {

    private static final String TAG = ArticleOnlineSearchRepo.class.getName();

    /**
     * loads sync Wikipedia Articles
     *
     * @param latitude
     * @param longitude
     * @param radius
     * @return
     * @throws IOException
     */
    public static List<WikipediaArticle> loadWikipediaArticles(double latitude, double longitude, int limit , int radius) throws IOException {

        java.util.List<WikipediaArticle> wikipediaArticleList = new ArrayList<WikipediaArticle>() ;

        String baseUrl = getWikipediaBaseUrl();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IWikipediaNearbySearch wikipediaNearbySearch = retrofit.create(IWikipediaNearbySearch.class);

        StringBuilder gscoord = new StringBuilder();
        gscoord.append(Double.toString(latitude));
        gscoord.append("|");
        gscoord.append(Double.toString(longitude));

        Call<WikipediaNearbyRespone> responeCall = wikipediaNearbySearch.searchWikipediaNearby("query" , "geosearch" , gscoord.toString() , "json" ,  limit  , radius) ;

        Response<WikipediaNearbyRespone> response = responeCall.execute();

        if(response.isSuccessful()) {
            WikipediaNearbyRespone wikipediaNearbyRespone = response.body();

            Log.d(TAG, "Batchcomplete " + wikipediaNearbyRespone.getBatchcomplete());

            for ( WikipediaNearbyResponseItem item  : wikipediaNearbyRespone.getQuery().getGeosearch() ) {
                WikipediaArticle wikipediaArticle = new WikipediaArticle();
                wikipediaArticle.setLat(item.getLat());
                wikipediaArticle.setLon(item.getLon());
                wikipediaArticle.setPageid(item.getPageid());
                wikipediaArticle.setTitle(item.getTitle());
                wikipediaArticle.setDistance(item.getDist());
                wikipediaArticle.setLink(baseUrl + "?curid=" + item.getPageid());

                wikipediaArticleList.add(wikipediaArticle) ;
            }

        } else {
            Log.w(TAG, "response not successful") ;
        }

        return wikipediaArticleList;

    }


    /**
     * https://de.wikipedia.org/wiki/Wikipedia:Sprachen
     */
    private static String getWikipediaBaseUrl() {
        final String DEFAULT_LANGUAGE = "en" ;
        final String[] SUPPORTED_LANGUAGES = { DEFAULT_LANGUAGE , "de" ,  "fr" , "sv" , "nl" , "es" , "it" , "pl" , "pt" , "uk"} ;

        String lang = Locale.getDefault().getLanguage();

        if(!Arrays.asList(SUPPORTED_LANGUAGES).contains(lang)) {
            lang = DEFAULT_LANGUAGE ;
        }

        return  "https://" + lang + ".wikipedia.org/";

    }

}
