package de.mchme.wikiarticlesnearby.service.articlepersistence;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(tableName = "articles" , primaryKeys = { "pageid" })
public class WikipediaArticleEntity {

    @NonNull
    public String pageid ;

    @NonNull
    public String title ;

    @NonNull
    public double lat ;

    @NonNull
    public double lon ;

    @NonNull
    public String link ;

    @NonNull
    public long Insertvalue ;


}
