package de.mchme.wikiarticlesnearby.ui.main;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;

public class MainViewModel extends ViewModel {

    private MutableLiveData<List<WikipediaArticle>> articleList ;

    public MutableLiveData<List<WikipediaArticle>> getArticleList() {
        if(articleList == null) {
            articleList = new MutableLiveData<List<WikipediaArticle>>();
        }
        return articleList;
    }
}