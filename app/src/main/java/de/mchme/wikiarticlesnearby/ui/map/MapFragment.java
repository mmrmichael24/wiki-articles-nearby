package de.mchme.wikiarticlesnearby.ui.map;

import android.content.Context;

import android.graphics.Bitmap;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer;
import org.osmdroid.bonuspack.utils.BonusPackHelper;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.ArrayList;
import java.util.List;

import de.mchme.wikiarticlesnearby.R;
import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;
import de.mchme.wikiarticlesnearby.ui.BaseFragment;
import de.mchme.wikiarticlesnearby.ui.main.MainViewModel;

/**
 *
 */
public class MapFragment extends BaseFragment {

    private static final String TAG = MapFragment.class.getName();


    public MapFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.map_menu, menu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.map_fragment, container, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.show_as_list:
                Navigation.findNavController(getView()).navigate(R.id.action_nav_map_to_list);
                break;

            default:
                break;
        }

        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mViewModel.getArticleList().observe(getActivity(), new Observer<List<WikipediaArticle>>() {
            @Override
            public void onChanged(List<WikipediaArticle> wikipediaArticles) {
                Log.d(TAG , "changed articlelist model");
                showMarker(wikipediaArticles);
                try {
                    zoomToBoundingBox(wikipediaArticles);
                } catch (NotEnoughPointsForBoundigBoxException nex) {
                    Log.w(TAG, "could not zoom to Bounding Box");
                }
            }
        });

        initMap();
        loadArticlesFromDatabase();

    }

    protected void initMap() {
        Context ctx = getActivity().getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        MapView map = getView().findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);

    }


    protected void showMarker(List<WikipediaArticle> wikipediaArticles) {

        MapView map = getView().findViewById(R.id.map);

        RadiusMarkerClusterer radiusMarkerClusterer = new RadiusMarkerClusterer(getContext()) ;
        Bitmap clusterIcon = BonusPackHelper.getBitmapFromVectorDrawable(getContext(), R.drawable.marker_cluster);
        radiusMarkerClusterer.setIcon(clusterIcon);

        map.getOverlayManager().overlays().add(radiusMarkerClusterer);

        for(WikipediaArticle wikipediaArticle : wikipediaArticles) {
            GeoPoint position = new GeoPoint( wikipediaArticle.getLat() , wikipediaArticle.getLon()  ) ;

            Marker marker = new Marker(map);
            marker.setTitle(wikipediaArticle.getTitle());
            marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            marker.setPosition(position);
            marker.setImage(getResources().getDrawable(R.drawable.btn_moreinfo, getContext().getTheme())) ;

            CustomInfoWindow customInfoWindow = new CustomInfoWindow(map, wikipediaArticle);
            marker.setInfoWindow(customInfoWindow);

            radiusMarkerClusterer.add(marker);
        }

        map.invalidate();
    }

    protected void zoomToBoundingBox(List<WikipediaArticle> wikipediaArticles) throws NotEnoughPointsForBoundigBoxException {

        List<GeoPoint> points = new ArrayList<GeoPoint>() ;
        MapView map = getView().findViewById(R.id.map);
        List<WikipediaArticle> filteredList = getFilteredArticleListByDistance(wikipediaArticles, DISPLAY_RADIUS_IN_METER) ;

        for(WikipediaArticle wikipediaArticle : filteredList) {
            GeoPoint position = new GeoPoint( wikipediaArticle.getLat() , wikipediaArticle.getLon()  ) ;
            points.add(position) ;
        }

        if(points.size() <= 1) {
            throw new NotEnoughPointsForBoundigBoxException();
        }

        BoundingBox boundingBox = BoundingBox.fromGeoPoints(points) ;
        boundingBox = getAdjustedBoundingBox(boundingBox) ;
        map.zoomToBoundingBox(boundingBox, true);
        map.invalidate();
    }

    private static BoundingBox getAdjustedBoundingBox(BoundingBox b ) {

        final double BOX_ADJUST = 0.0025;

        BoundingBox box = b ;

        box.setLonEast(box.getLonEast() + BOX_ADJUST);
        box.setLonWest(box.getLonWest() - BOX_ADJUST);

        box.setLatNorth(box.getLatNorth() + BOX_ADJUST);
        box.setLatSouth(box.getLatSouth() - BOX_ADJUST);

        return box ;
    }

    class NotEnoughPointsForBoundigBoxException extends Exception {

    }

}