package de.mchme.wikiarticlesnearby.ui.map;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;

/**
 * https://github.com/MKergall/osmbonuspack/wiki/Tutorial_2#7-customizing-the-bubble-behaviour
 */
public class CustomInfoWindow extends MarkerInfoWindow {

    private static final String TAG = CustomInfoWindow.class.getName();

    public CustomInfoWindow(MapView mapView, WikipediaArticle wikipediaArticle) {
        super(org.osmdroid.bonuspack.R.layout.bonuspack_bubble, mapView);

        ImageView bubbleImage = getView().findViewById(org.osmdroid.bonuspack.R.id.bubble_image) ;

        bubbleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, wikipediaArticle.getTitle());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(wikipediaArticle.getLink()));
                view.getContext().startActivity(intent);
            }
        });


    }



}
