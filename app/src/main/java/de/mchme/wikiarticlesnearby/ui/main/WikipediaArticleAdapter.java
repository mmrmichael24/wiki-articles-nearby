package de.mchme.wikiarticlesnearby.ui.main;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.mchme.wikiarticlesnearby.R;
import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;


/**
 * https://guides.codepath.com/android/using-the-recyclerview
 */
public class WikipediaArticleAdapter extends    RecyclerView.Adapter<WikipediaArticleAdapter.ViewHolder> implements Filterable  {

    private static final String TAG = WikipediaArticleAdapter.class.getName();

    final private List<WikipediaArticle> allWikipediaArticleList ;
    private List<WikipediaArticle> filteredWikipediaArticleList ;

    public WikipediaArticleAdapter(List<WikipediaArticle> wikipediaArticles) {
        this.allWikipediaArticleList = wikipediaArticles;
        this.filteredWikipediaArticleList = wikipediaArticles;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.article_list_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final DecimalFormat df2 = new DecimalFormat("#.##");

        WikipediaArticle wikipediaArticle = this.filteredWikipediaArticleList.get(position) ;
        TextView textViewName = holder.nameTextView ;
        textViewName.setText(wikipediaArticle.getTitle());

        TextView textViewDistance = holder.distanceTextView ;

        double distanceInKm = wikipediaArticle.getDistance() / 1000.0;
        textViewDistance.setText(df2.format(distanceInKm) + " km");
    }

    @Override
    public int getItemCount() {
        return this.filteredWikipediaArticleList.size();
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<WikipediaArticle> list = allWikipediaArticleList;

                List<WikipediaArticle> nlist = new ArrayList<WikipediaArticle>();

                for (int i = 0; i < list.size(); i++) {
                    WikipediaArticle item = list.get(i);

                    if (item.getTitle().toLowerCase().contains(filterString)) {
                        nlist.add(item);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredWikipediaArticleList = ( List<WikipediaArticle> ) results.values;
                notifyDataSetChanged();
            }
        } ;

        return filter;

    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView nameTextView;
        public TextView distanceTextView ;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTextView =  itemView.findViewById(R.id.article_title);
            distanceTextView =  itemView.findViewById(R.id.article_distance);
            ImageView mapButton =  itemView.findViewById(R.id.article_map) ;

            LinearLayout articleInfoLayout = itemView.findViewById(R.id.article_infos_layout) ;

            articleInfoLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick Link") ;
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        WikipediaArticle wa = filteredWikipediaArticleList.get(position) ;
                        Log.d(TAG, wa.getLink()) ;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(wa.getLink()));
                        view.getContext().startActivity(intent);
                    }
                }
            });


           mapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick Map") ;
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        WikipediaArticle wa = filteredWikipediaArticleList.get(position) ;
                        // https://developers.google.com/maps/documentation/urls/android-intents
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String positionData = String.format("geo: %s,%s" , wa.getLat(), wa.getLon());
                        String navigationData = String.format(" ?q= %s,%s" , wa.getLat(), wa.getLon());
                        String data = positionData+navigationData;
                        intent.setData(Uri.parse(data));
                        view.getContext().startActivity(intent);
                    }
                }
            });
        }


    }

}
