package de.mchme.wikiarticlesnearby.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fonfon.geohash.GeoHash;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import de.mchme.wikiarticlesnearby.R;
import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;
import de.mchme.wikiarticlesnearby.service.repo.ArticleDatabaseRepo;
import de.mchme.wikiarticlesnearby.service.repo.ArticleOnlineSearchRepo;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 *  Please see
 */
public class MainFragment extends Fragment {

    private final int RC_FINE_LOCATION = 1;
    private final String[] fineLocationPerms = {Manifest.permission.ACCESS_FINE_LOCATION};

    private static final String TAG = MainFragment.class.getName();

    private final int SEARCH_RADIUS_IN_METER = 3550 ;
 //   private final double DISPLAY_RADIUS_IN_METER = 3500.0 ;

    private final int SEARCH_CACHE_IN_HOURS = 4 ;
    private final int ARTICLE_CACHE_IN_HOURS = 72 ;

    private MainViewModel mViewModel;
    private SwipeRefreshLayout swipeRefreshLayout ;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.show_as_map:
                Navigation.findNavController(getView()).navigate(R.id.action_nav_main_to_show_map);
                break;

            default:
                break;
        }

        return true;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mViewModel.getArticleList().observe(getActivity(), new Observer<List<WikipediaArticle>> () {
            @Override
            public void onChanged(List<WikipediaArticle> wikipediaArticles) {
                Log.d(TAG , "changed articlelist model");
                updateArticleList(wikipediaArticles);
            }
        });

        // https://www.geeksforgeeks.org/pull-to-refresh-with-listview-in-android-with-example/
       swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshArticlesLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchForArticlesNearby();
            }
        });

        showHint();
        loadArticlesFromDatabase();

        SearchView searchView = getView().findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                RecyclerView recyclerView = getView().findViewById(R.id.rvArticles);
                WikipediaArticleAdapter adapter = (WikipediaArticleAdapter)recyclerView.getAdapter();
                adapter.getFilter().filter(newText);

                return false;
            }
        });

    }


    private void showHint() {
        Snackbar.make( getView()  , R.string.pull_to_refresh, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Search online for Articles
     */
    protected void searchForArticlesNearby() throws SecurityException {
        if ( hasLocationPermissions() ) {
            Log.d(TAG, "has location permissions") ;

            FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.getActivity());

            Task<Location> locationTask = fusedLocationClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, null);

            locationTask.addOnSuccessListener(this.getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                Log.d(TAG, "get current location");

                                ExecutorService executor = Executors.newSingleThreadExecutor() ;
                                executor.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            String geohash = getGeoHash(location) ;
                                            Log.d(TAG, "got Geohash " + geohash) ;
                                            Boolean cachExpired = ArticleDatabaseRepo.isCacheExpired(geohash, SEARCH_CACHE_IN_HOURS, getContext()) ;
                                            if(cachExpired) {
                                                Log.d(TAG, "cache expired, looking online for new Articles") ;
                                                List<WikipediaArticle> wikipediaArticleList = ArticleOnlineSearchRepo.loadWikipediaArticles(location.getLatitude(), location.getLongitude(), 100, SEARCH_RADIUS_IN_METER);
                                                Log.d(TAG, "got " + wikipediaArticleList.size() + " Articles online back");
                                                ArticleDatabaseRepo.deleteArticlesByTime(ARTICLE_CACHE_IN_HOURS, getContext());
                                                Log.d(TAG , "deleted article cache") ;
                                                ArticleDatabaseRepo.insertNewWikiArticles(wikipediaArticleList, getContext());
                                                Log.d(TAG, "insert new articles into database");
                                                ArticleDatabaseRepo.insertNewHistoryEntry(geohash, wikipediaArticleList.size(), getContext());
                                            }
                                            loadArticlesFromDatabase();
                                        } catch (IOException ioe) {
                                            Log.e(TAG, ioe.getMessage()) ;
                                        } finally {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    swipeRefreshLayout.setRefreshing(false);
                                                    executor.shutdown();
                                                }
                                            });
                                        }
                                    }
                                });

                            } else {
                                Log.w(TAG, "could not get current location");
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    }
            );

        } else {
            EasyPermissions.requestPermissions(MainFragment.this, "test", RC_FINE_LOCATION, fineLocationPerms);
        }
    }

    /**
     * Load the Articles from the database
     */
    protected void loadArticlesFromDatabase() {
        Log.d(TAG, "entering loadArticlesFromDatabase");
        ExecutorService executor = Executors.newSingleThreadExecutor() ;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    List<WikipediaArticle> wikipediaArticleList = ArticleDatabaseRepo.getWikipediaArticles(getContext());
                    Log.d(TAG, "got " + wikipediaArticleList.size() + " Articles from database back");
                    updateArticlesWithDistanceInfo(wikipediaArticleList);
                } finally {
                    executor.shutdown();
                }
            }
        });
    }

    /**
     * check the last known location to get the distance between User and the Article
     *
     */
    protected void updateArticlesWithDistanceInfo(List<WikipediaArticle> wikipediaArticles) throws SecurityException {
        if(hasLocationPermissions()) {
            Log.d(TAG, "trying to get last location") ;

                FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.getActivity());

                fusedLocationClient.getLastLocation().addOnSuccessListener(this.getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            Log.d(TAG, "got last location");

                            for (WikipediaArticle wa : wikipediaArticles) {
                                Location l = new Location("test");
                                l.setLongitude(wa.getLon());
                                l.setLatitude(wa.getLat());

                                wa.setDistance(location.distanceTo(l));
                            }
                        } else {
                            Log.w(TAG, "could not get last location");
                        }
                        mViewModel.getArticleList().postValue(wikipediaArticles);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "got failure of last location " + e.getMessage());
                        mViewModel.getArticleList().postValue(wikipediaArticles);
                    }
                });

        } else {
            mViewModel.getArticleList().postValue(wikipediaArticles);
        }
    }

    /**
     * updates the Recyclerview
     *
     * @param wikipediaArticles
     */
    protected void updateArticleList( List<WikipediaArticle> wikipediaArticles) {
        Log.d(TAG, "entering updateArticleList");
        RecyclerView rvArticles = getView().findViewById(R.id.rvArticles);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvArticles.addItemDecoration(itemDecoration);

        List<WikipediaArticle> sortedList = wikipediaArticles.stream().sorted(Comparator.comparingDouble(WikipediaArticle::getDistance)).collect(Collectors.toList());

   //     List<WikipediaArticle> filteredList = sortedList.stream().filter( x -> x.getDistance() <= DISPLAY_RADIUS_IN_METER).collect(Collectors.toList());

        WikipediaArticleAdapter adapter = new WikipediaArticleAdapter(sortedList);
        rvArticles.setAdapter(adapter);

        rvArticles.setLayoutManager(new LinearLayoutManager(getContext()));
    }




//
// Permissions
//
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @AfterPermissionGranted(RC_FINE_LOCATION)
    protected void permissionsGranted() {
        Log.d(TAG, "permissions granted");
        searchForArticlesNearby();
    }

    private boolean hasLocationPermissions() {
        boolean hasPermissions = false ;

        if(EasyPermissions.hasPermissions(this.getContext(), fineLocationPerms)) {
            hasPermissions = true;
        }

        return hasPermissions;
    }

    // Geohash
    // https://en.wikipedia.org/wiki/Geohash
    protected String getGeoHash( Location location ) {
        return GeoHash.fromLocation(location , 6).toString();
    }


}
