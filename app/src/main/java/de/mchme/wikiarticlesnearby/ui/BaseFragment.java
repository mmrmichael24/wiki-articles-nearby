package de.mchme.wikiarticlesnearby.ui;

import android.Manifest;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import de.mchme.wikiarticlesnearby.dao.WikipediaArticle;
import de.mchme.wikiarticlesnearby.service.repo.ArticleDatabaseRepo;
import de.mchme.wikiarticlesnearby.ui.main.MainViewModel;
import pub.devrel.easypermissions.EasyPermissions;

public abstract class BaseFragment extends Fragment  {

    private static final String TAG = BaseFragment.class.getName();

    protected final String[] fineLocationPerms = {Manifest.permission.ACCESS_FINE_LOCATION};

    protected final int SEARCH_RADIUS_IN_METER = 3550 ;
    protected final double DISPLAY_RADIUS_IN_METER = 3500.0 ;

    protected final int SEARCH_CACHE_IN_HOURS = 4 ;
    protected final int ARTICLE_CACHE_IN_HOURS = 72 ;

    protected MainViewModel mViewModel;


    protected final boolean hasLocationPermissions() {
        boolean hasPermissions = EasyPermissions.hasPermissions(this.getContext(), fineLocationPerms);

        return hasPermissions;
    }

    /**
     * Load the Articles from the database
     */
    protected void loadArticlesFromDatabase() {
        Log.d(TAG, "entering loadArticlesFromDatabase");
        ExecutorService executor = Executors.newSingleThreadExecutor() ;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    List<WikipediaArticle> wikipediaArticleList = ArticleDatabaseRepo.getWikipediaArticles(getContext());
                    Log.d(TAG, "got " + wikipediaArticleList.size() + " Articles from database back");
                    updateArticlesWithDistanceInfo(wikipediaArticleList);
                } finally {
                    executor.shutdown();
                }
            }
        });
    }

    /**
     * check the last known location to get the distance between User and the Article
     *
     * @param wikipediaArticles
     * @throws SecurityException
     */
    protected void updateArticlesWithDistanceInfo(List<WikipediaArticle> wikipediaArticles) throws SecurityException {
        if(hasLocationPermissions()) {
            Log.d(TAG, "trying to get last location") ;

            FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.getActivity());

            fusedLocationClient.getLastLocation().addOnSuccessListener(this.getActivity(), new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        Log.d(TAG, "got last location");

                        for (WikipediaArticle wa : wikipediaArticles) {
                            Location l = new Location("test");
                            l.setLongitude(wa.getLon());
                            l.setLatitude(wa.getLat());

                            wa.setDistance(location.distanceTo(l));
                        }
                    } else {
                        Log.w(TAG, "could not get last location");
                    }
                    mViewModel.getArticleList().postValue(wikipediaArticles);
                }
            }).addOnFailureListener(e -> {
                Log.w(TAG, "got failure of last location " + e.getMessage());
                mViewModel.getArticleList().postValue(wikipediaArticles);
            });

        } else {
            mViewModel.getArticleList().postValue(wikipediaArticles);
        }
    }

    protected final List<WikipediaArticle>  getFilteredArticleListByDistance( List<WikipediaArticle> wikipediaArticles, double distance) {

        List<WikipediaArticle> sortedList = wikipediaArticles.stream().sorted(Comparator.comparingDouble(WikipediaArticle::getDistance)).collect(Collectors.toList());

        return sortedList.stream().filter(x -> x.getDistance() <= distance).collect(Collectors.toList());
    }

}
